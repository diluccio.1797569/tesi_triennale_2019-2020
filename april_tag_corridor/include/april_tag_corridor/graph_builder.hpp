#ifndef __GRAPH_BUILDER_HPP__
#define __GRAPH_BUILDER_HPP__

#include <string>
#include <fstream>
#include <vector>
#include <functional>
#include <unordered_map>
#include <cmath>
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <tf/LinearMath/Matrix3x3.h>
#include "april_tag_corridor/AprilTagDetection.h"
#include "april_tag_corridor/AprilTagDetectionArray.h"
#include "april_tag_corridor/GetFixingPoints.h"

namespace thesis
{
#define START_ODOM_ID 1100

    extern unsigned aprilTagDetectionIdMap[16];
    extern bool aprilTagDetectedMap[16];
    
    class GraphBuilder
    {
    private:
        ros::NodeHandle &nh;
        ros::Subscriber odomSub;
        ros::Subscriber aprTagDetectSub;
        tf::TransformListener tfSub;
        ros::ServiceServer fixServer;
        std::ofstream poseVerticesFout;
        std::ofstream poseEdgesFout;
        std::ofstream tagVerticesFout;
        std::ofstream tagEdgesFout;
        nav_msgs::Odometry lastOdomMsg;
        std::unordered_map<unsigned, tf::Pose> writtenPosesMap;
        std::unordered_map<unsigned, unsigned> poseIdsMap;
        unsigned odomMsgId;
        bool isFirstPoseVertex;

        bool mustWriteOdom(const tf::Pose &pose1, const tf::Pose &pose2);
    public:
        GraphBuilder(ros::NodeHandle &_nh, const char *poseVerticesFileName,
            const char *poseEdgesFileName, const char *tagVerticesFileName,
            const char *tagEdgesFileName);
        ~GraphBuilder();
        void onOdom(const nav_msgs::Odometry &odomMsg);
        void onAprTagDetect(const april_tag_corridor::AprilTagDetectionArray
            &aprTagDetectArrayMsg);
        bool onGetFixingPoints(april_tag_corridor::GetFixingPoints::Request &reqMsg,
            april_tag_corridor::GetFixingPoints::Response &resMsg);
    };
}

#endif