#ifndef __APRIL_TAG_DETECTOR_HPP__
#define __APRIL_TAG_DETECTOR_HPP__

#include <sstream>
#include <vector>
#include <ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_datatypes.h>
#include <tf/LinearMath/Vector3.h>
#include <tf/LinearMath/Matrix3x3.h>
#include "april_tag_corridor/AprilTagDetection.h"
#include "april_tag_corridor/AprilTagDetectionArray.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Geometry>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <apriltag/apriltag.h>
#include <apriltag/tagCircle21h7.h>
#include <apriltag/apriltag_pose.h>
#include <apriltag/common/homography.h>

namespace thesis
{

#define TAG_SIZE 0.08
#define FX 269.852539
#define FY 269.733262
#define CX 157.051246
#define CY 113.117878

    class AprilTagDetector
    {
    private:
        inline void addObjectPoints(double s, cv::Matx44d T_oi,
            std::vector<cv::Point3d> &objectPoints) const;
        inline void addImagePoints(apriltag_detection_t *detectionPtr,
            std::vector<cv::Point2d> &imagePoints) const;
        void computeTransformation(std::vector<cv::Point3d> &objectPoints,
            std::vector<cv::Point2d> &imagePoints, double fx, double fy, double cx, double cy,
            Eigen::Matrix4d &T);
        inline void computeTagPose(const Eigen::Matrix4d &T, const std_msgs::Header &imgHeader,
            geometry_msgs::PoseStamped &pose);
        void drawDetections(cv::Mat &image, apriltag_detection_t *detectionPtr,
            const geometry_msgs::PoseStamped &pose, std::stringstream &ssout);
        void detectTags(const cv::Mat &imageToDetect, cv::Mat &imageToDraw,
            const std_msgs::Header &imgHeader, const nav_msgs::Odometry &robotOdometry,
            april_tag_corridor::AprilTagDetectionArray &aprTagDetectArrayMsg);

        ros::NodeHandle &nh;
        image_transport::ImageTransport imgTr;
        message_filters::Subscriber<sensor_msgs::Image> imageSub;
        message_filters::Subscriber<nav_msgs::Odometry> odomSub;

        typedef message_filters::sync_policies::ApproximateTime<
            sensor_msgs::Image, nav_msgs::Odometry
        > MySyncPolicy;
        
        message_filters::Synchronizer<MySyncPolicy> sync;
        image_transport::Publisher imagePub;
        ros::Publisher detectPub;
        apriltag_family_t *aprTagFamPtr;
        apriltag_detector_t *aprTagDetectPtr;
    public:
        explicit AprilTagDetector(ros::NodeHandle &_nh);
        ~AprilTagDetector();
        void onImageOdom(const sensor_msgs::ImageConstPtr &imageMsg,
            const nav_msgs::OdometryConstPtr &odomMsg);
    };
}

#endif