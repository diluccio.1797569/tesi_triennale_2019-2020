#ifndef __APRIL_TAG_DETECTOR_CLIENT_HPP__
#define __APRIL_TAG_DETECTOR_CLIENT_HPP__

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

namespace thesis
{
    class AprilTagDetectorClient
    {
    private:
        ros::NodeHandle &nh;
        image_transport::ImageTransport imgTr;
        image_transport::Subscriber imgSub;
    public:
        explicit AprilTagDetectorClient(ros::NodeHandle &_nh);
        ~AprilTagDetectorClient();
        void onImage(const sensor_msgs::ImageConstPtr &imageMsg);
    };
}

#endif