#ifndef __ODOM_ROBOT_TRANSFORM_PUBLISHER_HPP__
#define __ODOM_ROBOT_TRANSFORM_PUBLISHER_HPP__

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>

namespace thesis
{
    class OdomRobotTransformPublisher
    {
    private:
        ros::NodeHandle &nh;
        ros::Subscriber odomSub;
        tf::TransformBroadcaster odomRobotBasePub;
    public:
        explicit OdomRobotTransformPublisher(ros::NodeHandle &_nh);
        ~OdomRobotTransformPublisher();
        void onOdom(const nav_msgs::Odometry &odomMsg);
    };
}

#endif