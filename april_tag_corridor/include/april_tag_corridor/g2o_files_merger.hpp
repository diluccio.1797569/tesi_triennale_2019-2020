#ifndef __G2O_FILES_MERGER_HPP__
#define __G2O_FILES_MERGER_HPP__

#include <fstream>
#include <string>
#include <ros/ros.h>
#include "april_tag_corridor/GetFixingPoints.h"

namespace thesis
{
    class G2OFilesMerger
    {
    private:
        ros::NodeHandle &nh;
        ros::ServiceClient fixClient;
        std::ofstream fout;
        std::ifstream fin;
        const char *fileOutName;
    public:
        explicit G2OFilesMerger(ros::NodeHandle &_nh, const char *_fileOutName);
        ~G2OFilesMerger();
        void merge(const char *fileInName);
        void addFixingPoints();
    };
}

#endif