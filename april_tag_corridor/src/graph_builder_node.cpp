#include <csignal>
#include <ros/ros.h>
#include "april_tag_corridor/graph_builder.hpp"

bool goOn;

void sigintHandler(int sigNum)
{
	goOn = false;
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "graph_builder_node", ros::init_options::NoSigintHandler);

	ros::NodeHandle nh{};

	ROS_INFO("Installing custom SIGINT handler...");
	if (std::signal(SIGINT, sigintHandler) == SIG_ERR)
	{
		ROS_ERROR("Failed to install custom SIGINT handler! :(");
		ROS_INFO("Aborting...");
		return -1;
	}
	ROS_INFO("Custom SIGINT handler installed with success! :)");
	goOn = true;

	thesis::GraphBuilder builder{nh, "/home/lorenzo/pose_vertices.g2o",
		"/home/lorenzo/pose_edges.g2o", "/home/lorenzo/tag_vertices.g2o",
		"/home/lorenzo/tag_edges.g2o"};

	while (goOn)
        ros::spinOnce();
	return 0;
}