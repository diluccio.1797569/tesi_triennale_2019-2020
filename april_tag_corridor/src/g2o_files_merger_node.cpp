#include <ros/ros.h>
#include "april_tag_corridor/g2o_files_merger.hpp"

int main(int argc, char **argv)
{
	ros::init(argc, argv, "g2o_files_merger_node");

	ros::NodeHandle nh{};
    thesis::G2OFilesMerger fMerger{nh, "/home/lorenzo/april_tag_graph.g2o"};

	fMerger.merge("/home/lorenzo/tag_vertices.g2o");
	fMerger.merge("/home/lorenzo/pose_vertices.g2o");
	fMerger.merge("/home/lorenzo/tag_edges.g2o");
    fMerger.merge("/home/lorenzo/pose_edges.g2o");
	fMerger.addFixingPoints();
	return 0;
}