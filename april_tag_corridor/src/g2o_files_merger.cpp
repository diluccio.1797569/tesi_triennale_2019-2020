#include "april_tag_corridor/g2o_files_merger.hpp"

namespace thesis
{
    G2OFilesMerger::G2OFilesMerger(ros::NodeHandle &_nh, const char *_fileOutName) :
        nh{_nh},
        fixClient{nh.serviceClient<april_tag_corridor::GetFixingPoints>(
            "/get_fixing_points_service")},
        fout{},
        fin{},
        fileOutName{_fileOutName}
    {
        ROS_INFO("Creating the G2O files merger...");
        ROS_INFO("Opening '%s' file as output...", _fileOutName);
        this->fout.open(_fileOutName);
        ROS_INFO("'%s' file opened as output with success! :)", _fileOutName);
        ROS_INFO("G2O files merger created with success! :)");
    }

    G2OFilesMerger::~G2OFilesMerger()
    {
        ROS_INFO("Destroying the G2O files merger...");
        ROS_INFO("Closing '%s' file...", this->fileOutName);
        this->fout.close();
        ROS_INFO("'%s file closed with success! :)", this->fileOutName);
        ROS_INFO("G2O files merger destroyed with success! :)");
    }

    void G2OFilesMerger::merge(const char *fileInName)
    {
        ROS_INFO("Opening '%s' file as input...", fileInName);
        this->fin.open(fileInName);
        ROS_INFO("'%s' file opened as input with success! :)", fileInName);
        ROS_INFO("Merging...");

        std::string line{};

        while (! this->fin.eof())
        {
            std::getline(this->fin, line);
            if (line.length() > 1)
                this->fout << line << std::endl;
        }
        ROS_INFO("Merged with success! :)");
        ROS_INFO("Closing '%s' file...", fileInName);
        this->fin.close();
        ROS_INFO("'%s' file closed with success! :)", fileInName);
    }

    void G2OFilesMerger::addFixingPoints()
    {
        ROS_INFO("Adding fixing points...");

        april_tag_corridor::GetFixingPoints fixSrv{};

        if (this->fixClient.call(fixSrv))
        {
            this->fout << "FIX " << fixSrv.response.firstFixPoint << std::endl <<
                "FIX " << fixSrv.response.lastFixPoint << std::endl;
            ROS_INFO("Fixing points added with success! :)");
        }
        else
            ROS_INFO("Failed to add fixing points! :(");
    }
}