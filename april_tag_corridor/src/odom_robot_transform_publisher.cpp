#include "april_tag_corridor/odom_robot_transform_publisher.hpp"

namespace thesis
{
    OdomRobotTransformPublisher::OdomRobotTransformPublisher(ros::NodeHandle &_nh) :
        nh{_nh},
        odomSub{nh.subscribe("/odom", 1000, &OdomRobotTransformPublisher::onOdom, this)},
        odomRobotBasePub{}
    {
        ROS_INFO("Creating the odom-robot transform publisher...");
        ROS_INFO("Odom-robot transform publisher created with success! :)");
    }

    OdomRobotTransformPublisher::~OdomRobotTransformPublisher()
    {
        ROS_INFO("Destroying the odom-robot transform publisher...");
        ROS_INFO("Odom-robot transform publisher destroyed with success! :)");
    }

    void OdomRobotTransformPublisher::onOdom(const nav_msgs::Odometry &odomMsg)
    {
        tf::Vector3 t{odomMsg.pose.pose.position.x, odomMsg.pose.pose.position.y,
            odomMsg.pose.pose.position.z};
        tf::Quaternion q{odomMsg.pose.pose.orientation.x, odomMsg.pose.pose.orientation.y,
            odomMsg.pose.pose.orientation.z, odomMsg.pose.pose.orientation.w};
        tf::Transform tr{q, t};
        tf::StampedTransform stTr{tr, ros::Time::now(), odomMsg.header.frame_id,
            "/robot_base"};

        this->odomRobotBasePub.sendTransform(stTr);
    }
}