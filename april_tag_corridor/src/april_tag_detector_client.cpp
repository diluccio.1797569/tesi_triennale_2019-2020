#include "april_tag_corridor/april_tag_detector_client.hpp"

namespace thesis
{
    AprilTagDetectorClient::AprilTagDetectorClient(ros::NodeHandle &_nh) :
        nh{_nh},
        imgTr{nh},
        imgSub{imgTr.subscribe("/april_tag/image_with_detections", 1000,
            &AprilTagDetectorClient::onImage, this)}
    {
        ROS_INFO("Creating the april tag detector client...");
        ROS_INFO("Opening the window...");
        cv::namedWindow("April Tag corridor");
        cv::waitKey(10);
        ROS_INFO("Window opened with success! :)");
        ROS_INFO("April tag detector client created with success! :)");
    }

    AprilTagDetectorClient::~AprilTagDetectorClient()
    {
        ROS_INFO("Destroying the april tag detector client...");
        ROS_INFO("Closing the window...");
        cv::destroyAllWindows();
        ROS_INFO("Window closed with success! :)");
        ROS_INFO("April tag detector client destroyed with success! :)");
    }

    void AprilTagDetectorClient::onImage(const sensor_msgs::ImageConstPtr &imageMsg)
    {
        cv::imshow("April Tag corridor", cv_bridge::toCvShare(imageMsg, "mono8")->image);
        cv::waitKey(10);
    }
}