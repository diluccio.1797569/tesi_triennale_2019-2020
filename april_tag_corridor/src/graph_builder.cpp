#include "april_tag_corridor/graph_builder.hpp"

namespace thesis
{
    unsigned aprilTagDetectionIdMap[16] = {6, 0, 3, 0, 0, 8, 0, 1, 0, 2, 0, 0, 7, 4, 0, 5};
    bool aprilTagDetectedMap[16] = {false, false, false, false, false, false, false, false,
        false, false, false, false, false, false, false, false};
    
    bool GraphBuilder::mustWriteOdom(const tf::Pose &pose1, const tf::Pose &pose2)
    {
        double dx = pose2.getOrigin().x() - pose1.getOrigin().x(),
            dy = pose2.getOrigin().y() - pose1.getOrigin().y(),
            dtheta = tf::getYaw(pose2.getRotation()) - tf::getYaw(pose1.getRotation());

        return dx * dx + dy * dy > 0.01 || dtheta > 0.3;
    }

    GraphBuilder::GraphBuilder(ros::NodeHandle &_nh, const char *poseVerticesFileName,
        const char *poseEdgesFileName, const char *tagVerticesFileName,
        const char *tagEdgesFileName) :
        nh{_nh},
        odomSub{nh.subscribe("/odom", 1000, &GraphBuilder::onOdom, this)},
        aprTagDetectSub{nh.subscribe("/april_tag/detections", 1000,
            &GraphBuilder::onAprTagDetect, this)},
        tfSub{nh},
        fixServer{nh.advertiseService("/get_fixing_points_service",
            &GraphBuilder::onGetFixingPoints, this)},
        poseVerticesFout{},
        poseEdgesFout{},
        tagVerticesFout{},
        tagEdgesFout{},
        lastOdomMsg{},
        writtenPosesMap{},
        poseIdsMap{},
        odomMsgId{START_ODOM_ID},
        isFirstPoseVertex{true}
    {
        ROS_INFO("Creating the graph builder...");
        this->poseVerticesFout.open(poseVerticesFileName);
        this->poseEdgesFout.open(poseEdgesFileName);
        this->tagVerticesFout.open(tagVerticesFileName);
        this->tagEdgesFout.open(tagEdgesFileName);
        ROS_INFO("Graph builder created with success! :)");
    }

    GraphBuilder::~GraphBuilder()
    {
        ROS_INFO("Destroying the graph builder...");
        this->poseVerticesFout.close();
        this->poseEdgesFout.close();
        this->tagVerticesFout.close();
        this->tagEdgesFout.close();
        ROS_INFO("Graph builder destroyed with success! :)");
    }

    void GraphBuilder::onOdom(const nav_msgs::Odometry &odomMsg)
    {
        tf::Pose pose{}, lastPose{};

        tf::poseMsgToTF(odomMsg.pose.pose, pose);
        tf::poseMsgToTF(this->lastOdomMsg.pose.pose, lastPose);
        if (this->isFirstPoseVertex || this->mustWriteOdom(lastPose, pose))
        {
            this->poseVerticesFout << "VERTEX_SE2 " << this->odomMsgId << " "
                        << pose.getOrigin().x() << " "
                        << pose.getOrigin().y() << " "
                        << tf::getYaw(pose.getRotation()) << std::endl;
            if (this->isFirstPoseVertex)
                this->isFirstPoseVertex = false;
            else
                this->poseEdgesFout << "EDGE_SE2 " << this->odomMsgId - 1 << " "
                    << this->odomMsgId << " "
                    << pose.getOrigin().x() - lastPose.getOrigin().x() << " "
                    << pose.getOrigin().y() - lastPose.getOrigin().y() << " "
                    << tf::getYaw(pose.getRotation()) - tf::getYaw(lastPose.getRotation()) << " "
                    << 500 << " " << 0 << " " << 0 << " " << 500 << " " << 0 << " " << 5000
                    << std::endl;
            this->writtenPosesMap.insert(std::make_pair(odomMsg.header.seq, pose));
            this->poseIdsMap.insert(std::make_pair(odomMsg.header.seq, (this->odomMsgId)++));
            this->lastOdomMsg = odomMsg;
        }
    }

    void GraphBuilder::onAprTagDetect(const april_tag_corridor::AprilTagDetectionArray
        &aprTagDetectMsg)
    {
        for (const april_tag_corridor::AprilTagDetection &item : aprTagDetectMsg.detections)
        {
            std::string camFrame{"/head_camera"}, odomFrame{"/odom"}, robotFrame{"/robot_base"};
            ros::Time time = ros::Time{0};
            tf::Pose camPose{}, odomPose{}, robotPose{};

            tf::poseMsgToTF(item.pose.pose, camPose);

            tf::Stamped<tf::Pose> stCamPose{camPose, time, camFrame},
                stOdomPose{odomPose, time, odomFrame}, stRobotPose{robotPose, time, robotFrame};
            try
            {
                tfSub.transformPose(odomFrame, stCamPose, stOdomPose);
            }
            catch (const tf::LookupException &e)
            {
                ROS_ERROR("Failed to transform the April Tag pose from frame '%s'"
                    " to frame '%s'! :(", camFrame.c_str(), odomFrame.c_str());
                return;
            }
            try
            {
                tfSub.transformPose(robotFrame, stCamPose, stRobotPose);
            }
            catch (const tf::LookupException &e)
            {
                ROS_ERROR("Failed to transform the April Tag pose from frame '%s'"
                    " to frame '%s'! :(", camFrame.c_str(), robotFrame.c_str());
                return;
            }
            
            tf::Pose robotOdomPose{};

            tf::poseMsgToTF(item.robotOdometry.pose.pose, robotOdomPose);

            std::unordered_map<unsigned, tf::Pose>::const_iterator it =
                this->writtenPosesMap.find(item.robotOdometry.header.seq);
            
            if (! aprilTagDetectedMap[item.id])
            {
                this->tagVerticesFout << "VERTEX_XY " << aprilTagDetectionIdMap[item.id] << " "
                    << stOdomPose.getOrigin().x() << " "
                    << stOdomPose.getOrigin().y() << std::endl;
                aprilTagDetectedMap[item.id] = true;
            }
            if (it != this->writtenPosesMap.cend())
            {
                std::unordered_map<unsigned, unsigned>::const_iterator it2 =
                    this->poseIdsMap.find(item.robotOdometry.header.seq);

                this->tagEdgesFout << "EDGE_SE2_XY " << it2->second << " "
                    << aprilTagDetectionIdMap[item.id] << " "
                    << stRobotPose.getOrigin().x() << " "
                    << stRobotPose.getOrigin().y() << " "
                    << 1000 << " " << 0 << " " << 1000 << std::endl;
                this->writtenPosesMap.erase(it);
                this->poseIdsMap.erase(it2);
            }
        }
    }

    bool GraphBuilder::onGetFixingPoints(april_tag_corridor::GetFixingPoints::Request &reqMsg,
        april_tag_corridor::GetFixingPoints::Response &resMsg)
    {
        resMsg.firstFixPoint = START_ODOM_ID;
        resMsg.lastFixPoint = this->odomMsgId - 1;
        return true;
    }
}
