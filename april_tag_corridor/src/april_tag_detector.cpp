#include "april_tag_corridor/april_tag_detector.hpp"
#include <iostream>

namespace thesis
{
    void AprilTagDetector::addObjectPoints(double s, cv::Matx44d T_oi,
        std::vector<cv::Point3d> &objectPoints) const
        
    {
        cv::Matx34d minor{T_oi.get_minor<3, 4>(0, 0)};

        objectPoints.push_back(minor * cv::Vec4d(-s, -s, 0, 1));
        objectPoints.push_back(minor * cv::Vec4d(s, -s, 0, 1));
        objectPoints.push_back(minor * cv::Vec4d{s, s, 0, 1});
        objectPoints.push_back(minor * cv::Vec4d{-s, s, 0, 1});
    }

    void AprilTagDetector::addImagePoints(apriltag_detection_t *detectionPtr,
        std::vector<cv::Point2d> &imagePoints) const
    {
        double tagXs[4] = {-1, 1, 1, -1}, tagYs[4] = {1, 1, -1, -1};
        double imgX, imgY;

        for (unsigned i = 0; i < 4; ++i)
        {
            homography_project(detectionPtr->H, tagXs[i], tagYs[i], &imgX, &imgY);
            imagePoints.push_back(cv::Point2d{imgX, imgY});
        }
    }

    void AprilTagDetector::computeTransformation(std::vector<cv::Point3d> &objectPoints,
        std::vector<cv::Point2d> &imagePoints, double fx, double fy, double cx, double cy,
        Eigen::Matrix4d &T)
    {
        cv::Mat rVector{}, tVector{};
        cv::Matx33d r{};
        Eigen::Matrix3d rEigen{};
        cv::Matx33d camMat{fx, 0, cx,
                            0, fy, cy,
                            0, 0, 1};
        
        cv::solvePnP(objectPoints, imagePoints, camMat, cv::Vec4f{0, 0, 0, 0}, rVector, tVector);
        cv::Rodrigues(rVector, r);

        rEigen << r(0, 0), r(0, 1), r(0, 2),
                  r(1, 0), r(1, 1), r(1, 2),
                  r(2, 0), r(2, 1), r(2, 2);

        T.topLeftCorner(3, 3) = rEigen;
        T.col(3).head(3) << tVector.at<double>(0),
                            tVector.at<double>(1),
                            tVector.at<double>(2);
        T.row(3) << 0, 0, 0, 1;
    }

    void AprilTagDetector::computeTagPose(const Eigen::Matrix4d &T,
        const std_msgs::Header &imgHeader, geometry_msgs::PoseStamped &pose)
    {
        Eigen::Matrix3d r{T.block(0, 0, 3, 3)};
        Eigen::Quaternion<double> q{r};
        Eigen::Vector3d t{T(0, 3), T(1, 3), T(2, 3)};

        pose.header = imgHeader;
        pose.pose.position.x = t.x();
        pose.pose.position.y = t.y();
        pose.pose.position.z = t.z();
        pose.pose.orientation.x = q.x();
        pose.pose.orientation.y = q.y();
        pose.pose.orientation.z = q.z();
        pose.pose.orientation.w = q.w();
    }

    void AprilTagDetector::drawDetections(cv::Mat &image, apriltag_detection_t *detectionPtr,
        const geometry_msgs::PoseStamped &pose, std::stringstream &ssout)
    {
        ssout.str("");
        ssout << "apriltag id: " << detectionPtr->id << std::flush;
        ssout << "(x: " << pose.pose.position.x
            << ", y: " << pose.pose.position.y
            << ")" << std::flush;

        cv::String text{ssout.str().c_str()};
        int baseline;
        cv::Size textSize{cv::getTextSize(text, CV_FONT_NORMAL, 2.0, 1, &baseline)};

        cv::line(image,
            cv::Point2d{detectionPtr->p[0][0], detectionPtr->p[0][1]},
            cv::Point2d{detectionPtr->p[1][0], detectionPtr->p[1][1]},
            cv::Scalar{0.0, 255.0, 0.0}, 2);
        cv::line(image,
            cv::Point2d{detectionPtr->p[0][0], detectionPtr->p[0][1]},
            cv::Point2d{detectionPtr->p[3][0], detectionPtr->p[3][1]},
            cv::Scalar{0.0, 255.0, 0.0}, 2);
        cv::line(image,
            cv::Point2d{detectionPtr->p[1][0], detectionPtr->p[1][1]},
            cv::Point2d{detectionPtr->p[2][0], detectionPtr->p[2][1]},
            cv::Scalar{0.0, 255.0, 0.0}, 2);
        cv::line(image,
            cv::Point2d{detectionPtr->p[2][0], detectionPtr->p[2][1]},
            cv::Point2d{detectionPtr->p[3][0], detectionPtr->p[3][1]},
            cv::Scalar{0.0, 255.0, 0.0}, 2);
        cv::putText(image, text,
            cv::Point2d{detectionPtr->c[0], detectionPtr->c[1]}, CV_FONT_NORMAL, 0.5,
            cv::Scalar{255.0, 153.0, 0.0}, 1);
    }

    void AprilTagDetector::detectTags(const cv::Mat &imageToDetect, cv::Mat &imageToDraw,
        const std_msgs::Header &imgHeader, const nav_msgs::Odometry &robotOdometry,
        april_tag_corridor::AprilTagDetectionArray &aprTagDetectArrayMsg)
    {
        image_u8_t imageAprTag = {.width = imageToDetect.cols, .height = imageToDetect.rows,
            .stride = imageToDetect.cols, .buf = imageToDetect.data};
        zarray_t *detectionsPtr = apriltag_detector_detect(this->aprTagDetectPtr, &imageAprTag);
        std::vector<cv::Point3d> objectPoints{};
        std::vector<cv::Point2d> imagePoints{};
        std::stringstream ssout{};

        aprTagDetectArrayMsg.header = imgHeader;

        for (unsigned i = 0; i < zarray_size(detectionsPtr); ++i)
        {
            apriltag_detection_t *detectionPtr;

            zarray_get(detectionsPtr, i, &detectionPtr);
            this->addObjectPoints(TAG_SIZE / 2, cv::Matx44d::eye(), objectPoints);
            this->addImagePoints(detectionPtr, imagePoints);

            Eigen::Matrix4d T{};

            this->computeTransformation(objectPoints, imagePoints, FX, FY, CX, CY, T);
            
            geometry_msgs::PoseStamped aprTagPose{};

            this->computeTagPose(T, imgHeader, aprTagPose);

            april_tag_corridor::AprilTagDetection aprTagDetectMsg{};

            aprTagDetectMsg.family = this->aprTagFamPtr->name;
            aprTagDetectMsg.id = detectionPtr->id;
            aprTagDetectMsg.pose = aprTagPose;
            aprTagDetectMsg.robotOdometry = robotOdometry;

            aprTagDetectArrayMsg.detections.push_back(aprTagDetectMsg);

            this->drawDetections(imageToDraw, detectionPtr, aprTagPose, ssout);
        }
    }

    AprilTagDetector::AprilTagDetector(ros::NodeHandle &_nh) :
        nh{_nh},
        imgTr{nh},
        imageSub{nh, "/usb_cam/image_raw", 1000},
        odomSub{nh, "/odom", 1000},
        sync{MySyncPolicy{10000}, imageSub, odomSub},
        imagePub{imgTr.advertise("/april_tag/image_with_detections", 1000)},
        detectPub{nh.advertise<april_tag_corridor::AprilTagDetectionArray>(
            "/april_tag/detections", 1000)},
        aprTagFamPtr{tagCircle21h7_create()},
        aprTagDetectPtr{apriltag_detector_create()}
    {
        sync.registerCallback(boost::bind(&AprilTagDetector::onImageOdom, this, _1, _2));
        ROS_INFO("Creating the april tag detector...");
        apriltag_detector_add_family(this->aprTagDetectPtr, this->aprTagFamPtr);
        ROS_INFO("April tag detector created with success! :)");
    }

    AprilTagDetector::~AprilTagDetector()
    {
        ROS_INFO("Destroying the april tag detector...");
        apriltag_detector_destroy(this->aprTagDetectPtr);
        tagCircle21h7_destroy(this->aprTagFamPtr);
        ROS_INFO("April tag detector destroyed with success! :)");
    }

    void AprilTagDetector::onImageOdom(const sensor_msgs::ImageConstPtr &imageMsg,
        const nav_msgs::OdometryConstPtr &odomMsg)
    {
        cv_bridge::CvImageConstPtr imagePtr = cv_bridge::toCvShare(imageMsg, "mono8");
        cv::Mat image = imagePtr->image;

        cv::Mat betterImage{};
        
        cv::adaptiveThreshold(image, betterImage, 255.0,
            CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY, 11, 5.0);
        cv::GaussianBlur(betterImage, betterImage, cv::Size{5, 5}, 0.0, 0.0);

        april_tag_corridor::AprilTagDetectionArray aprTagDetectArrayMsg{};

        this->detectTags(betterImage, image, imageMsg->header, *odomMsg, aprTagDetectArrayMsg);

        this->imagePub.publish(imagePtr->toImageMsg());
        if (! aprTagDetectArrayMsg.detections.empty())
            this->detectPub.publish(aprTagDetectArrayMsg);
    }
}