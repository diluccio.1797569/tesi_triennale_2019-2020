#include <csignal>
#include <ros/ros.h>
#include "april_tag_corridor/april_tag_detector.hpp"

bool goOn;

void sigintHandler(int sigNum)
{
	goOn = false;
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "april_tag_detector_node", ros::init_options::NoSigintHandler);

	ros::NodeHandle nh{};

	ROS_INFO("Installing custom SIGINT handler...");
	if (std::signal(SIGINT, sigintHandler) == SIG_ERR)
	{
		ROS_ERROR("Failed to install custom SIGINT handler! :(");
		ROS_INFO("Aborting...");
		return -1;
	}
	ROS_INFO("Custom SIGINT handler installed with success! :)");
	goOn = true;

	thesis::AprilTagDetector aprTagDet{nh};

	while (goOn)
		ros::spinOnce();
	return 0;
}